<?php

use Behat\Gherkin\Node\TableNode;
use Behat\Mink\Exception\ExpectationException;


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends AttractorContext
{

    /**
     * @When /^я вижу слово "([^"]*)" на странице$/
     * @param $arg
     */
    public function ISeeTheWordWhereThatIsOnPage($arg)
    {
        $this->assertPageContainsText($arg);
    }

    /**
     * @When /^я вижу, что на страница не выбросила ошибку с исключением$/
     */
    public function thereIsNoException()
    {
        $this->assertPageNotContainsText('Symfony Exception');
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function ImOnTheMainPage()
    {
        $this->visit($this->getContainer()->get('router')->generate('homepage'));
    }


    /**
     * @When /^я заполняю поля данными$/
     * @param TableNode $fields
     * @return null
     */
    public function fillFields(TableNode $fields)
    {

        foreach ($fields->getRowsHash() as $field => $value) {
            $this->fillField($field, $value);
        }

        return $fields;

    }

    /**
     * Attaches file to field with specified id|name|label|value.
     *
     * @When /^я добавляю в дирректорию"([^"]*)" файл "([^"]*)"$/
     * @param $field
     * @param $path
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function attachFileToField($field, $path)
    {
        $field = $this->fixStepArgument($field);

        if ($this->getMinkParameter('files_path')) {
            $fullPath = rtrim(realpath($this->getMinkParameter('files_path')), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $path;
            if (is_file($fullPath)) {
                $path = $fullPath;
            }
        }

        $this->getSession()->getPage()->attachFileToField($field, $path);
    }

    /**
     * @When /^я нажимаю на кнопку "([^"]*)"$/
     * @param $button
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function pressButton($button)
    {
        $button = $this->fixStepArgument($button);
        $this->getSession()->getPage()->pressButton($button);
    }

    /**
     * @Then /^Я нажимаю на элемент "([^"]*)"$/
     * @param $element
     * @throws Exception
     */
    public function iClickOn($element)
    {
        $page = $this->getSession()->getPage();
        $findName = $page->find("css", $element);
        if (!$findName) {
            throw new Exception($element . " could not be found");
        } else {
            $findName->click();
        }
    }

    /**
     * @When /^я кликаю по ссылке с id "([^"]*)"/
     * @param $link
     */
    public function iClickOnTheText($link)
    {
        $this->getSession()->getPage()->find('css', '#' . $link)->click();
    }


    /**
     * @When /^я перехожу в панель администратора$/
     */
    public function ImOnTheAdminPage()
    {
        $this->visit($this->getContainer()->get('router')->generate('sonata_admin_redirect'));
    }

    /**
     * @When /^я перехожу на страницу "([^"]*)"$/
     * @param $route
     */
    public function IAmGoingToPage($route)
    {
        $this->visit($this->getContainer()->get('router')->generate($route));
    }

    /**
     * @When /^я заполняю поле "([^"]*)" данными "([^"]*)"$/
     * @param $field
     * @param $value
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function fillField($field, $value)
    {
        $field = $this->fixStepArgument($field);
        $value = $this->fixStepArgument($value);
        $this->getSession()->getPage()->fillField($field, $value);
    }


    /**
     *
     * @When /^Я нажимаю на ссылку "((?:[^"]|\\")*)"$/
     * @param $link
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function clickLink($link)
    {
        $link = $this->fixStepArgument($link);
        $this->getSession()->getPage()->clickLink($link);
    }

    /**
     * @Then я пишу в поле wysiwyg :locator значение :value
     * @param $locator
     * @param $value
     * @throws Exception
     * @throws ExpectationException
     */
    public function iFillInWysiwygOnFieldWith($locator, $value)
    {
        $el = $this->getSession()->getPage()->findField($locator);

        if (empty($el)) {
            throw new ExpectationException('Could not find WYSIWYG with locator: ' . $locator, $this->getSession());
        }

        $fieldId = $el->getAttribute('id');

        if (empty($fieldId)) {
            throw new Exception('Could not find an id for field with locator: ' . $locator);
        }
        $this->getSession()->executeScript("tinymce.get()[0].setContent('" . $value . "');");
    }


    /**
     * @Then /^Я вижу cлово "([^"]*)" в элементе "([^"]*)"$/
     * @param $word
     * @param $selector
     * @throws Exception
     * @internal param $element
     */
    public function assertElementText($word, $selector)
    {
        $page = $this->getSession()->getPage();
        $elements = $page->findAll("css", $selector);
        foreach ($elements as $element) {
            if (strpos(strtolower($element->getText()), strtolower($word)) !== false) {
                return;
            }
        }
        throw new Exception("Text '{$word}' is not found in the elements.");
    }

    /**
     * @Then /^Я вижу элемент "([^"]*)"$/
     * @param $selector
     * @throws Exception
     * @internal param $element
     */
    public function assertElement($selector)
    {
        $this->assertElementOnPage($selector);
    }

    /**
     * @When /^я перехожу на страницу "([^"]*)" с параметром "([^"]*)" и значением "([^"]*)"$/
     * @param $route
     * @param $paramName
     * @param $paramValue
     * @internal param $param
     */
    public function iAmGoingToPageWithParam($route, $paramName, $paramValue)
    {
        $this->visit($this->getContainer()->get('router')->generate($route, array($paramName => $paramValue)));
    }

    /** @BeforeScenario @loginAdmin
     *
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function loginAdminBeforeScenario()
    {
        $this->visitPath($this->getContainer()->get('router')->generate('fos_user_security_login'));
        $this->fillField('username', 'admin@gmail.com');
        $this->fillField('password', '123');
        $this->pressButton('Войти');
    }

    /** @BeforeScenario @loginUser
     *
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function loginUserBeforeScenario()
    {
        $this->visitPath($this->getContainer()->get('router')->generate('fos_user_security_login'));
        $this->fillField('username', 'morty@gmail.com');
        $this->fillField('password', 'qwerty');
        $this->pressButton('Войти');
    }

    /** @BeforeScenario @loginEditedUser
     *
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function loginEditedUserBeforeScenario()
    {
        $this->visitPath($this->getContainer()->get('router')->generate('fos_user_security_login'));
        $this->fillField('username', 'mail@mail.ru');
        $this->fillField('password', 'qwerty');
        $this->pressButton('Войти');
    }

    /**
     * @AsterScenario @logout
     */
    public function logoutAfterScenario()
    {
        $this->visitPath($this->getContainer()->get('router')->generate('fos_user_security_logout'));
    }

    /**
     * @BeforeFeature @fixtures
     */
    static public function loadFixtures()
    {
        exec('php bin/console doctrine:database:drop --force');
        exec('php bin/console doctrine:database:create');
        exec('php bin/console doctrine:schema:create');
        exec('cp -r fixturesData/images public/uploads');
        exec('php bin/console doctrine:fixtures:load -n');
    }

    /**
     * @Then /^Я нажимаю на выпадающий список "([^"]*)" и выбираю "([^"]*)"$/
     * @param $select
     * @param $option
     */
    public function selectOption($select, $option)
    {
        $page = $this->getSession()->getPage();
        $page->find("css", $select)->selectOption($option);
    }

    /**
     * @Then /^Я жду "([^"]*)" секунд"$/
     * @param $seconds
     */
    public function iWillWait($seconds)
    {
        sleep($seconds);
    }

    /**
     * @Then /^Я отправлю форму с id "([^"]*)"$/
     */
    public function iSubmitTheFormWithId($arg)
    {
        $node = $this->getSession()->getPage()->find('css', $arg);
        if($node) {
            $this->getSession()->executeScript("jQuery('$arg').submit();");
        } else {
            throw new Exception('Element not found');
        }
    }

}
