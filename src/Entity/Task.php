<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 29.09.18
 * Time: 9:56
 */

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 */
class Task
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=1024)
     * @var string
     */
    private $text;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $full_text;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $create_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $public_date;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $important_task;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tasks")
     */
    private $author;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="tasks")
     */
    private $category;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="tasks")
     */
    private $tags;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="task")
     */
    private $comments;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\LikeItem", mappedBy="task")
     */
    private $likeItems;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->likeItems = new ArrayCollection();
        $this->important_task = false;
        $this->create_date = new \DateTime('now');
    }

    public function __toString()
    {
        return $this->text;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getFullText(): ?string
    {
        return $this->full_text;
    }

    public function setFullText(?string $full_text): self
    {
        $this->full_text = $full_text;

        return $this;
    }

    public function getCreateDate(): ?\DateTimeInterface
    {
        return $this->create_date;
    }

    public function setCreateDate(\DateTimeInterface $create_date): self
    {
        $this->create_date = $create_date;

        return $this;
    }

    public function getPublicDate(): ?\DateTimeInterface
    {
        return $this->public_date;
    }

    public function setPublicDate(?\DateTimeInterface $public_date): self
    {
        $this->public_date = $public_date;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setTask($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getTask() === $this) {
                $comment->setTask(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LikeItem[]
     */
    public function getLikeItems(): Collection
    {
        return $this->likeItems;
    }

    public function addLikeItem(LikeItem $likeItem): self
    {
        if (!$this->likeItems->contains($likeItem)) {
            $this->likeItems[] = $likeItem;
            $likeItem->setTask($this);
        }

        return $this;
    }

    public function removeLikeItem(LikeItem $likeItem): self
    {
        if ($this->likeItems->contains($likeItem)) {
            $this->likeItems->removeElement($likeItem);
            // set the owning side to null (unless already changed)
            if ($likeItem->getTask() === $this) {
                $likeItem->setTask(null);
            }
        }

        return $this;
    }

    /**
     * @param bool $important_task
     * @return Task
     */
    public function setImportantTask(bool $important_task): Task
    {
        $this->important_task = $important_task;
        return $this;
    }

    /**
     * @return bool
     */
    public function isImportantTask(): bool
    {
        return $this->important_task;
    }
}