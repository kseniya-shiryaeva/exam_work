<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function findWithSortAndLimit(string $today, int $offset, int $limit)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.public_date <= :val')
            ->setParameter('val', $today)
            ->orderBy('t.public_date', 'DESC')
            ->setFirstResult( $offset )
            ->setMaxResults( $limit )
            ->getQuery()
            ->getResult()
            ;
    }

    public function findWithSortAndLimitAndCategory(string $today, int $offset, int $limit, int $category)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.public_date <= :val')
            ->andWhere('t.category = :cat')
            ->setParameter('val', $today)
            ->setParameter('cat', $category)
            ->orderBy('t.public_date', 'DESC')
            ->setFirstResult( $offset )
            ->setMaxResults( $limit )
            ->getQuery()
            ->getResult()
            ;
    }

    public function findWithSortAndLimitAndCategoryAndTag(string $today, int $offset, int $limit, int $category, int $tag)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.public_date <= :val')
            ->andWhere('t.category = :cat')
            ->andWhere('t.tags LIKE :tag')
            ->setParameter('val', $today)
            ->setParameter('cat', $category)
            ->setParameter('tag', $tag)
            ->orderBy('t.public_date', 'DESC')
            ->setFirstResult( $offset )
            ->setMaxResults( $limit )
            ->getQuery()
            ->getResult()
            ;
    }

    public function findTaskCount(string $today)
    {
        return $this->createQueryBuilder('t')
            ->select('count(t.id)')
            ->Where('t.public_date <= :val')
            ->setParameter('val', $today)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findTaskCountByCategory(string $today, int $category)
    {
        return $this->createQueryBuilder('t')
            ->select('count(t.id)')
            ->Where('t.public_date <= :val')
            ->andWhere('t.category = :cat')
            ->setParameter('val', $today)
            ->setParameter('cat', $category)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findTaskCountByTag(string $today, int $tag)
    {
        return $this->createQueryBuilder('t')
            ->select('count(t.id)')
            ->Where('t.public_date <= :val')
            ->andWhere('t.tags LIKE :tag')
            ->setParameter('val', $today)
            ->setParameter('tag', $tag)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findTaskCountByCategoryAndTag(string $today, int $category, int $tag)
    {
        return $this->createQueryBuilder('t')
            ->select('count(t.id)')
            ->Where('t.public_date <= :val')
            ->andWhere('t.tags LIKE :tag')
            ->andWhere('t.category = :cat')
            ->setParameter('val', $today)
            ->setParameter('tag', $tag)
            ->setParameter('cat', $category)
            ->getQuery()
            ->getResult()
            ;
    }
}
