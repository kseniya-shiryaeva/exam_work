<?php

namespace App\DataFixtures;


use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category1
            ->setTitle('Политика');
        $manager->persist($category1);
        $this->addReference('category1', $category1);

        $category2 = new Category();
        $category2
            ->setTitle('ПДД');
        $manager->persist($category2);
        $this->addReference('category2', $category2);

        $category3 = new Category();
        $category3
            ->setTitle('События');
        $manager->persist($category3);
        $this->addReference('category3', $category3);

        $manager->flush();
    }

}