<?php

namespace App\Admin;

use App\Entity\Task;
use App\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CommentAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('text', null, array('label' => 'Текст'))
            ->add('task', null, array('label' => 'Новость'))
            ->add('create_date', null, array('label' => 'Дата создания'))
            ->add('author', null, array('label' => 'Автор'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('text', null, array('label' => 'Текст'))
            ->add('task', null, array('label' => 'Новость'))
            ->add('create_date', null, array('label' => 'Дата создания'))
            ->add('author', null, array('label' => 'Автор'))
            ->add('_action', null, array('label' => 'Действие',
                'actions' => array(
                    'delete' => array(),
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('text', TextType::class, array('label' => 'Текст'))
            ->add('task', EntityType::class, array('label' => 'Новость', 'class' => Task::class))
            ->add('author', EntityType::class, array('label' => 'Автор', 'class' => User::class))
        ;
    }
}