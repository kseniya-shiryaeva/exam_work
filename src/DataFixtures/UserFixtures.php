<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {
        $user1 = new User();
        $user1
            ->setUsername('user1')
            ->setEmail('user1@gmail.com')
            ->setPlainPassword('qwerty')
            ->setEnabled(true)
            ->setRoles(['ROLE_USER']);
        $manager->persist($user1);
        $this->addReference('user1', $user1);

        $user2 = new User();
        $user2
            ->setUsername('user2')
            ->setEmail('user2@gmail.com')
            ->setPlainPassword('qwerty')
            ->setEnabled(true)
            ->setRoles(['ROLE_USER']);
        $manager->persist($user2);
        $this->addReference('user2', $user2);

        $user3 = new User();
        $user3
            ->setUsername('user3')
            ->setEmail('user3@gmail.com')
            ->setPlainPassword('qwerty')
            ->setEnabled(true)
            ->setRoles(['ROLE_USER']);
        $manager->persist($user3);
        $this->addReference('user3', $user3);

        $admin = new User();
        $admin
            ->setUsername('admin')
            ->setEmail('admin@gmail.com')
            ->setPlainPassword('123')
            ->setEnabled(true)
            ->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);
        $this->addReference('admin', $admin);
        $manager->flush();
    }
}
