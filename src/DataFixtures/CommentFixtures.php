<?php

namespace App\DataFixtures;


use App\Entity\Comment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $user1 = $this->getReference('user1');
        $user2 = $this->getReference('user2');
        $user3 = $this->getReference('user3');
        $admin = $this->getReference('admin');

        $task1 = $this->getReference('task_1');
        $task2 = $this->getReference('task_2');
        $task3 = $this->getReference('task_3');


        $comments = [
            [
                'text' =>'Я комментирую новость',
                'author' => $user3,
                'create_date' =>'2018-09-07 13:51:58',
                'task' =>$task1
            ],
            [
                'text' =>'Я снова комментирую новость',
                'author' => $user3,
                'create_date' =>'2018-09-08 13:51:58',
                'task' =>$task1
            ],
            [
                'text' =>'Я опять комментирую новость',
                'author' => $user3,
                'create_date' =>'2018-09-07 13:51:58',
                'task' =>$task2
            ],
            [
                'text' =>'Я комментирую новость',
                'author' => $user1,
                'create_date' =>'2018-09-07 13:51:58',
                'task' =>$task1
            ],
            [
                'text' =>'Я комментирую новость',
                'author' => $user2,
                'create_date' =>'2018-09-07 13:51:58',
                'task' =>$task3
            ],
        ];

        foreach ($comments as $comment){
            $one_comment = new Comment();
            $one_comment
                ->setText($comment['text'])
                ->setAuthor($comment['author'])
                ->setCreateDate(new \DateTime($comment['create_date']))
                ->setTask($comment['task']);

            $manager->persist($one_comment);
        }



        $manager->flush();
    }

    public function getDependencies()

    {

        return array(

            UserFixtures::class,
            TaskFixtures::class

        );

    }

}