<?php

namespace App\Repository;

use App\Entity\LikeItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LikeItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method LikeItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method LikeItem[]    findAll()
 * @method LikeItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LikeItemRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LikeItem::class);
    }

    public function findTasksByTask($id)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.task = :task')
            ->setParameter('task', $id)
            ->getQuery()
            ->getResult()
            ;
    }

}
