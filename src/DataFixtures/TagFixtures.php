<?php

namespace App\DataFixtures;


use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TagFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $tag1 = new Tag();
        $tag1
            ->setTitle('бишкек');
        $manager->persist($tag1);
        $this->addReference('tag1', $tag1);

        $tag2 = new Tag();
        $tag2
            ->setTitle('кыргызстан');
        $manager->persist($tag2);
        $this->addReference('tag2', $tag2);

        $tag3 = new Tag();
        $tag3
            ->setTitle('важно');
        $manager->persist($tag3);
        $this->addReference('tag3', $tag3);

        $manager->flush();
    }

}