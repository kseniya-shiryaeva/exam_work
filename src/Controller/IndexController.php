<?php


namespace App\Controller;


use App\Entity\Comment;
use App\Entity\LikeItem;
use App\Entity\Task;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;


class IndexController extends Controller
{
    /**
     * @Route("/", name="homepage")
     *
     */
    public function indexAction(){

        return $this->redirectToRoute("news", ['page'=>1, 'limit'=>5]);

    }

    /**
     * @Route("/news/page/{page}/limit/{limit}/{category}/{tag}", defaults={"category"=0, "tag"=0}, name="news")
     *
     * @param int $page
     * @param int $limit
     * @param int $category
     * @param int $tag
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function newsAction(int $page, int $limit, int $category, int $tag, Request $request, ObjectManager $manager){

        $message = '';

        $offset = $limit*($page-1);

        $today = date('now');

        if($category > 0){
            $news_count = $this->getDoctrine()->getRepository('App:Task')->findTaskCountByCategory($today, $category);
        } else{
            $news_count = $this->getDoctrine()->getRepository('App:Task')->findTaskCount($today);
        }



        if($news_count[0][1] > $limit){
            $pages = [];
            for($i=1; $i <= ceil($news_count[0][1]/$limit); $i++){
                $pages[] = $i;
            }
        }

        if($category > 0){
            $news = $this->getDoctrine()->getRepository('App:Task')->findWithSortAndLimitAndCategory($today, $offset, $limit, $category);
        } else{
            $news = $this->getDoctrine()->getRepository('App:Task')->findWithSortAndLimit($today, $offset, $limit);
        }

        $task = new Task();
        $form = $this->createForm("App\Form\NewTaskType", $task);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task->setAuthor($this->getUser());

            $manager->persist($task);
            $manager->flush();

            $message = 'Новость успешно создана!';
        }

        return $this->render('index.html.twig',[
            'news' => $news,
            'pages' => $pages ?? null,
            'user' =>$this->getUser(),
            'form' => $form->createView(),
            'message' => $message,
            'category' => $category ?? null
        ]);
    }

    /**
     * @Route("/news_by_tag/{tag}", name="news_by_tag")
     *
     * @param int $tag
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newsByTagAction(int $tag, Request $request, ObjectManager $manager)
    {

       $news = $this->getDoctrine()->getRepository('App:Tag')->find($tag)->getTasks();

        $task = new Task();
        $form = $this->createForm("App\Form\NewTaskType", $task);

        $form->handleRequest($request);

        $message = '';

        if ($form->isSubmitted() && $form->isValid()) {
            $task->setAuthor($this->getUser());

            $manager->persist($task);
            $manager->flush();

            $message = 'Новость успешно создана!';
        }

        return $this->render('index.html.twig',[
            'news' => $news,
            'user' =>$this->getUser(),
            'form' => $form->createView(),
            'message' => $message,
            'category' => $category ?? null
        ]);

    }

    /**
     * @Route("/news/item/{id}", name="news_item")
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showNewsItemAction(int $id, Request $request, ObjectManager $manager){

        $message = '';

        $comments = $this->getDoctrine()->getRepository('App:Comment')->findByTask($id);

        $news_task = $this->getDoctrine()->getRepository('App:Task')->find($id);

        $comment = new Comment();
        $form = $this->createForm("App\Form\NewCommentType", $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setAuthor($this->getUser());
            $comment->setTask($news_task);

            $manager->persist($comment);
            $manager->flush();

            $message = 'Комментарий успешно добавлен!';
        }

        return $this->render('news_task.html.twig',[
            'news_task' => $news_task,
            'user' =>$this->getUser(),
            'form' => $form->createView(),
            'message' => $message,
            'comments' => $comments
        ]);
    }

    public function showCategoriesAction(){

        $categories = $this->getDoctrine()->getRepository('App:Category')->findAll();

        return $this->render('categories.html.twig', [
            'categories' => $categories
        ]);
    }


    public function showTagsAction(?int $category){

        $tags = $this->getDoctrine()->getRepository('App:Tag')->findAll();

        return $this->render('tags.html.twig', [
            'tags' => $tags
        ]);
    }

    /**
     * @Route("/news/category/{category}", name="news_category")
     *
     * @param $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function showNewsByCategory($category){

        return $this->redirectToRoute("news", [
            'page'=>1,
            'limit'=>5,
            'category'=>$category
        ]);
    }

    /**
     * @Route("/news/tag/{tag}", name="news_tag")
     *
     * @param $tag
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function showNewsByTag($tag){
        return $this->redirectToRoute("news", ['page'=>1, 'limit'=>5, 'tag'=>$tag]);
    }

    /**
     * @Route("/show_like_buttons/{task}", name="show_like_buttons")
     * @param int $task
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ShowLikeButtons(int $task){
        $like_info = [];

        $likes = $this->getDoctrine()->getRepository('App:Task')->find($task)->getLikeItems();

        $like_info['quality'] = 0;
        $like_info['actuality'] = 0;
        $like_info['happiness'] = 0;

        $like_info['quality_active'] = 1;
        $like_info['actuality_active'] = 1;
        $like_info['happiness_active'] = 1;


        foreach ($likes as $like){
            switch ($like->getType()){
                case 'quality':
                    $like_info['quality'] += $like->getVal();
                    break;
                case 'actuality':
                    $like_info['actuality'] += $like->getVal();
                    break;
                case 'happiness':
                    $like_info['happiness'] += $like->getVal();
                    break;
                default:
                    break;
            }

            if($like->getAuthor() == $this->getUser()){
                switch ($like->getType()){
                    case 'quality':
                        $like_info['quality_active'] = 0;
                        break;
                    case 'actuality':
                        $like_info['actuality_active'] = 0;
                        break;
                    case 'happiness':
                        $like_info['happiness_active'] = 0;
                        break;
                    default:
                        break;
                }
            }
        }

        return $this->render('like_buttons.html.twig', [
            'likes' => $like_info,
            'task' => $task
        ]);

    }


    /**
     * @Route("/profile", name="profile")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showProfileAction(){

        $rating = 0;

        $user = $this->getUser();

        $post_count = count($user->getTasks());

        if($post_count>0){

            $quality = 0;
            $actuality = 0;

            foreach ($user->getTasks() as $user_task){

                $likes = $this->getDoctrine()->getRepository('App:LikeItem')->findTasksByTask($user_task->getId());

                foreach ($likes as $like){
                    switch ($like->getType()){
                        case 'quality':
                            $quality += $like->getVal();
                            break;
                        case 'actuality':
                            $actuality += $like->getVal();
                            break;
                        default:
                            break;
                    }
                }
            }

            $rating = ($post_count+$quality+$actuality)/3;
        }





        return $this->render('profile.html.twig', [
            'user' => $user,
            'rating' => $rating
        ]);
    }


    /**
     * @Route("/make_like/task/{task}/type/{type}/val/{val}", name="make_like")
     * @param int $task
     * @param string $type
     * @param int $val
     * @param ObjectManager $objectManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function makeLikeAction(int $task, string $type, int $val, ObjectManager $objectManager){
        $like = new LikeItem();

        $task = $this->getDoctrine()->getRepository('App:Task')->find($task);

        $like
            ->setAuthor($this->getUser())
            ->setType($type)
            ->setTask($task)
            ->setVal($val);

        $objectManager->persist($like);
        $objectManager->flush();

        return $this->redirectToRoute("news", [
            'page'=>1,
            'limit'=>5]);
    }

}