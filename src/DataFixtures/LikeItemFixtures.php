<?php

namespace App\DataFixtures;


use App\Entity\LikeItem;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class LikeItemFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $user1 = $this->getReference('user1');
        $user2 = $this->getReference('user2');
        $user3 = $this->getReference('user3');

        $task1 = $this->getReference('task_1');


        $items = [
            [
                'type' =>'quality',
                'val' => 1,
                'task' => $task1,
                'author' => $user1
            ],
            [
                'type' =>'quality',
                'val' => 1,
                'task' => $task1,
                'author' => $user2
            ],
            [
                'type' =>'quality',
                'val' => -1,
                'task' => $task1,
                'author' => $user3
            ],
            [
                'type' =>'actuality',
                'val' => 1,
                'task' => $task1,
                'author' => $user1
            ],
            [
                'type' =>'actuality',
                'val' => 1,
                'task' => $task1,
                'author' => $user2
            ],
            [
                'type' =>'actuality',
                'val' => -1,
                'task' => $task1,
                'author' => $user3
            ],
            [
                'type' =>'happiness',
                'val' => 1,
                'task' => $task1,
                'author' => $user1
            ],
            [
                'type' =>'happiness',
                'val' => 1,
                'task' => $task1,
                'author' => $user2
            ],
            [
                'type' =>'happiness',
                'val' => -1,
                'task' => $task1,
                'author' => $user3
            ],
        ];

        foreach ($items as $item){
            $one_item = new LikeItem();
            $one_item
                ->setType($item['type'])
                ->setVal($item['val'])
                ->setTask($item['task'])
                ->setAuthor($item['author']);

            $manager->persist($one_item);
        }



        $manager->flush();
    }

    public function getDependencies()

    {

        return array(

            UserFixtures::class,
            TaskFixtures::class

        );

    }

}