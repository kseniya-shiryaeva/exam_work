<?php
namespace App\Form;

use App\Entity\Category;
use App\Entity\Tag;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class NewTaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'text',
                TextType::class, [
                    'label' => 'Краткий текст',
                ]
            )
            ->add(
                'full_text',
                TextareaType::class, [
                    'label' => 'Полный текст',
                ]
            )
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'label' => 'Категория'
            ])
            ->add('tags', EntityType::class, [
                'class' => Tag::class,
                'label' => 'Тэги',
                'multiple' => true
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Добавить'
            ]);

        ;
    }
}