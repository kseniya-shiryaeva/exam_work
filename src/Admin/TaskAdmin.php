<?php

namespace App\Admin;

use App\Entity\Category;
use App\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TaskAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('text', null, array('label' => 'Краткий текст'))
            ->add('full_text', null, array('label' => 'Полный текст'))
            ->add('create_date', null, array('label' => 'Дата создания'))
            ->add('public_date', null, array('label' => 'Дата публикации'))
            ->add('category', null, array('label' => 'Категория'))
            ->add('author', null, array('label' => 'Автор'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('text', null, array('label' => 'Краткий текст'))
            ->add('full_text', null, array('label' => 'Полный текст'))
            ->add('create_date', null, array('label' => 'Дата создания'))
            ->add('public_date', null, array('label' => 'Дата публикации'))
            ->add('category', EntityType::class, array('label' => 'Категория'))
            ->add('author', null, array('label' => 'Автор'))
            ->add('_action', null, array('label' => 'Действие',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('text', TextType::class, array('label' => 'Краткий текст'))
            ->add('full_text', TextareaType::class, array('label' => 'Полный текст'))
            ->add('create_date', DateType::class, array('label' => 'Дата создания',
                'widget' => 'single_text'
            ))
            ->add('public_date', DateType::class, array('label' => 'Дата публикации',
                'widget' => 'single_text'
            ))
            ->add('important_task', null, array('label' => 'Важная новость'))
            ->add('author', EntityType::class, array('label' => 'Автор', 'class' => User::class))
            ->add('category', EntityType::class, array('label' => 'Категория', 'class' => Category::class))
        ;
    }
}